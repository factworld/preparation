Dear all,

allen Beteiligten nochmals besten Dank für den offenen, intensiven und wie ich hoffe im Grossen klärenden Austausch am 11.1. in Frankfurt.

Ich habe den Kreis der Bitbucket-Nutzer erweitert und alle bisher Unberücksichtigten angemeldet. Haben nun alle die Einladungen erhalten? 

Wie wir festgestellt haben, hat es offenbar zu einigen Punkten unterschiedliche Vorstellungen gegeben, was von dem was für FACT benötigt wird, schon vorhanden ist bzw. inwieweit auf frühere Projekte und ihre Ergebnisse zurückgegriffen werden kann. Dies gilt insbesondere für das Anlagetool, welches eine noch ungetestete „Neukreation" und vollständig zu programmieren wäre, was eine vorherige Geeignetheitsprüfung in Form von Backtests und/oder Zukunftssimulationen vorläufig unmöglich macht. Wie besprochen wollen wir jetzt untersuchen, was und mit welchen Aufwand leistbar sei und welche Ressourcen, etwa Zugänge zu Datenbanken, Asset Management Expertise, etc., vorhanden sind bzw. ggf. fehlen. Wie erbeten stelle ich zur Vertiefung/weiteren Diskussion des Anforderungsprofils Hr. Riplinger die Mailkorrespondenz dazu aus den letzten neun Monaten (leite sie per Mail weiter).

Darüber hinaus haben wir noch einmal die KI Themen hervorgehoben, die uns einen signifikanten Mehrwert vis-a-vis den Kunden bieten und/oder die Leistungsfähigkeit/Wirtschaftlichkeit der Anwendung erhöhen könnten und uns möglicherweise von den Konkurrenzangeboten abheben. Dazu gehören Algorithmen zur a) Klassifizierung der Kunden, b) Gewinnung anlagerelevanter Informationen, c) Umwandlung dieser in Kundenkommunikation und d) Bots zur Kundeninteraktion. Wir wollen zeitnah untersuchen, wie diese umzusetzen wären und welcher Aufwand damit verbunden ist, um zu entscheiden, ob bzw. wie diese zu integrieren wären. Ggf. ob sie sich ganz oder in Teilen für den Prototypen eignen.  

In dem Zusammenhang wollen wir anhand von relevanten Usecases ein Anforderungsprofil für die Prototypengestaltung/-umsetzung definieren.
 
Wir haben beschlossen, dass wir in dem den Prototypen begleitenden Pitchdeck noch eine möglichst plakative Darstellung der KI Optionen und ihre Anwendungsrelevanz für FACT verankern werden, so dass die Potentiale ihrer Nutzung auch für die Investoren greifbar werden.

Herr Stürmer sagte zudem zeitnah den Entwurf eines Projektplans zu, den wir dann diskutieren können.

Sollte ich etwas vergessen haben oder anders erinnern, dann bitte ich um Hinweise.

HG,
RC









